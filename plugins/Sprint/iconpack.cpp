#include <QDebug>
#include <QJsonDocument>
#include <QFileInfo>
#include <QFile>
#include <QDir>

#include "iconpack.h"

IconPack::IconPack() {}

IconPack::IconPack(const QString &iconPackFile) {
    m_iconPackFile = iconPackFile;
    QFileInfo packInfo(m_iconPackFile);
    QString dirPath = packInfo.dir().absolutePath();

    QFile data(m_iconPackFile);
    data.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(data.readAll());
    QJsonObject iconPackData = doc.object();

    data.close();

    m_id = m_iconPackFile.remove(QStringLiteral("/opt/click.ubuntu.com/"));
    m_id.truncate(m_id.indexOf("/"));

    m_title = iconPackData.value(QStringLiteral("title")).toString().trimmed();
    m_author = iconPackData.value(QStringLiteral("author")).toString().trimmed();
    m_maintainer = iconPackData.value(QStringLiteral("maintainer")).toString().trimmed();
    m_description = iconPackData.value(QStringLiteral("description")).toString().trimmed();
    m_preview = dirPath + QStringLiteral("/") + iconPackData.value(QStringLiteral("preview")).toString().trimmed();
    m_icon = dirPath + QStringLiteral("/") + iconPackData.value(QStringLiteral("icon")).toString().trimmed();

    m_iconsPath = dirPath + QStringLiteral("/") + iconPackData.value(QStringLiteral("icons")).toString().trimmed() + QStringLiteral("/");
}

QString IconPack::id() const {
    return m_id;
}

QString IconPack::title() const {
    return m_title;
}

QString IconPack::author() const {
    return m_author;
}

QString IconPack::maintainer() const {
   return m_maintainer;
}

QString IconPack::description() const {
    return m_description;
}

QString IconPack::preview() const {
    return m_preview;
}

QString IconPack::icon() const {
    return m_icon;
}

void IconPack::loadIcons() {
    QFile pack(m_iconsPath + QStringLiteral("icon-pack.json"));
    pack.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(pack.readAll());
    m_icons = doc.object();

    pack.close();
}

QString IconPack::getIcon(const QString &appId) const {
    QString icon = m_icons.value(appId).toString().trimmed();
    if (!icon.isEmpty()) {
        icon = m_iconsPath + icon;
    }

    return icon;
}

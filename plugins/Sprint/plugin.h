#ifndef SPRINTPLUGIN_H
#define SPRINTPLUGIN_H

#include <QQmlExtensionPlugin>

class SprintPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif

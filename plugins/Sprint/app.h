#ifndef APP_H
#define APP_H

#define DESKTOP_FILE_KEY_NAME "Desktop Entry/Name"
#define DESKTOP_FILE_KEY_COMMENT "Desktop Entry/Comment"
#define DESKTOP_FILE_KEY_ICON "Desktop Entry/Icon"
#define DESKTOP_FILE_KEY_NO_DISPLAY "Desktop Entry/NoDisplay"
#define DESKTOP_FILE_KEY_APP_ID "Desktop Entry/X-Ubuntu-Application-ID"
#define DESKTOP_FILE_KEY_UBUNTU_TOUCH "Desktop Entry/X-Ubuntu-Touch"
#define DESKTOP_FILE_KEY_ONLY_SHOW_IN "Desktop Entry/OnlyShowIn"
#define DESKTOP_FILE_KEY_NOT_SHOW_IN "Desktop Entry/NotShowIn"

#include <QObject>
#include <QSettings>

class App: public QObject {
    Q_OBJECT

    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString comment READ comment CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)
    Q_PROPERTY(QString appId READ appId CONSTANT)
    Q_PROPERTY(QString uri READ uri CONSTANT)
    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString packageName READ packageName CONSTANT)
    Q_PROPERTY(QString version READ version CONSTANT)
    Q_PROPERTY(bool system READ system CONSTANT)

public:
    App();
    App(const QString desktopFile);
    ~App() = default;

    QString name() const;
    QString comment() const;
    QString icon() const;
    QString appId() const;
    QString id() const;
    QString packageName() const;
    QString uri() const;
    QString version() const;
    bool system() const;
    bool isVisible() const;

private:
    QString m_desktopFile;
    QSettings m_appInfo;
    bool m_system = false;
};

#endif

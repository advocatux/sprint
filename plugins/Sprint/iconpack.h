#ifndef ICONPACK_H
#define ICONPACK_H

#include <QObject>
#include <QJsonObject>

class IconPack: public QObject {
    Q_OBJECT

    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString title READ title CONSTANT)
    Q_PROPERTY(QString name READ title CONSTANT)
    Q_PROPERTY(QString author READ author CONSTANT)
    Q_PROPERTY(QString maintainer READ maintainer CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QString preview READ preview CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)

public:
    IconPack();
    IconPack(const QString &iconPackFile);
    ~IconPack() = default;

    QString id() const;
    QString title() const;
    QString author() const;
    QString maintainer() const;
    QString description() const;
    QString preview() const;
    QString icon() const;

    Q_INVOKABLE void loadIcons();
    Q_INVOKABLE QString getIcon(const QString &appId) const;

private:
    QString m_iconPackFile;
    QString m_id;
    QString m_title;
    QString m_author;
    QString m_maintainer;
    QString m_description;
    QString m_preview;
    QString m_icon;

    QString m_iconsPath;
    QJsonObject m_icons;
};

#endif
